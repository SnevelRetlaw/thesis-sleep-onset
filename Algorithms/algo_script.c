#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>


FILE *fp_acc;
char *filename_acc;

FILE *fp_HR;
char *filename_HR;

FILE *fp_result;
char *filename_result = "test_result.txt";

// add global constant for acc threshold (3.35mg)
const double ACC_THRESHOLD = 0.00335*9.81; //0.00335 * 9.81; 0.15 seems to be a nice value, where there is not a lot of MDs of 1
const int WINDOW_SIZE_ACC = 8;

struct time_and_value
{
    long int time;
    double value;
};

// add global variables for the raw data of Acc and HR
// 300 datapoints per epoch (10*30) and max 60 -ish epochs
struct time_and_value raw_acc_data[100][1000];
int total_acc_epochs;
// 30 datapoints per epoch (1 per second) and max 60 -ish epochs
struct time_and_value raw_hr_data[100][30];
int total_hr_epochs;

// add global variables for the processed data of acc and HR

int start_sleep_onset_period;

struct time_and_value acc_peaks[1000];
struct time_and_value filtered_acc_peaks[1000];
struct time_and_value all_filtered_acc_peaks[100][1000];

int activity_p2p[100];
double MD_p2p_array[100];

int activity_max_mag[100];
double MD_max_mag_array[100];

double MD_combined[100];

//TODO: maybe make epochs of HR and acc a global variable, so you can stop looping over MD array when last epoch is reached.
void parse_acc_file(){
    int epoch = 0;
    int data = 0;
    char * line = NULL;
    char * token;
    size_t len = 0;
    ssize_t read;
    // loop: read line in file until file end
    while ((read = getline(&line, &len, fp_acc)) != -1){
        if (line == ""){
            break;
        }
        // end of epoch reached
        if (strstr(line, "epoch")!= NULL){
            epoch++;
            data = 0;
        } else {
            // split variable in time and value variables
            token = strtok(line, ", ");
            long int timestamp = atoi(token);
            token = strtok(NULL,",");
            char *eptr;
            double value = strtod(token, &eptr);

            // store values in struct 
            struct time_and_value tav;
            tav.time = timestamp;
            tav.value = value;

            // store struct in array
            raw_acc_data[epoch][data] = tav;
            data++;
        }
    
    }
    total_acc_epochs = epoch;
    //printf("total_acc_epochs: %d, ", total_acc_epochs);

    // print values for debug
    // printf("\nData from accelerometer\n");
    // for (int i = 0; i<sizeof(raw_acc_data); i++){
    //     if (raw_acc_data[i][0].time == 0){
    //         break;
    //     }
    //     for (int j = 0; j<sizeof(raw_acc_data[i]);j++){
    //         if (raw_acc_data[i][j].time == 0){
    //             j = sizeof(raw_acc_data[i]);
    //         } else {
    //             printf("epoch %d, data %d: time %ld and value %f\n", i, j, raw_acc_data[i][j].time, raw_acc_data[i][j].value);
    //         }
    //     }
    // }
}

void parse_HR_file(){
    int epoch = 0;
    int data = 0;
    char * line = NULL;
    char * token;
    size_t len = 0;
    ssize_t read;
    // loop: read line in file until file end
    while ((read = getline(&line, &len, fp_HR)) != -1){
        if (line == ""){
            break;
        }
        // end of epoch reached
        if (strstr(line, "epoch")!= NULL){
            epoch++;
            data = 0;
        } else {
            // split variable in time and value variables
            token = strtok(line, ", ");
            long int timestamp = atoi(token);
            token = strtok(NULL,",");
            char *eptr;
            double value = strtod(token, &eptr);

            // store values in struct 
            struct time_and_value tav;
            tav.time = timestamp;
            tav.value = value;

            // store struct in array
            raw_hr_data[epoch][data] = tav;
            data++;
        }
    }
    total_hr_epochs = epoch;
    printf("total_hr_epochs: %d\n", total_hr_epochs);

    //print values for debug
    // printf("\nData from Heart rate\n");
    // for (int i = 0; i<sizeof(raw_hr_data); i++){
    //     if (raw_hr_data[i][0].time == 0){
    //         break;
    //     }
    //     for (int j = 0; j<sizeof(raw_hr_data[i]);j++){
    //         if (raw_hr_data[i][j].time == 0){
    //             j = sizeof(raw_hr_data[i]);
    //         } else {
    //             printf("epoch %d, data %d: time %d and value %f\n", i, j, raw_hr_data[i][j].time, raw_hr_data[i][j].value);
    //         }
    //     }
        
    // }
}

double calc_mean_acc(int epoch){
    double total = 0;
    double mean = 0;
    for (int i = 0; i<sizeof(raw_acc_data[epoch]);i++){
        if (raw_acc_data[epoch][i].time != 0){
            total += raw_acc_data[epoch][i].value;
        } else{
            mean = total / i;
            // printf("total: %f\n", total);
            i = sizeof(raw_acc_data[epoch]);
        }
    }
    return mean;
}

void filter_peaks(int epoch){
    struct time_and_value temp_arr[400];
    for(int i=0;i<sizeof(temp_arr)/sizeof(temp_arr[0]);i++){
        temp_arr[i].time = raw_acc_data[epoch][i].time;
        temp_arr[i].value = raw_acc_data[epoch][i].value;
        //printf("temp_arr[%d]: time %ld, value %lf\n",i,temp_arr[i].time, temp_arr[i].value);
    }
    //memcpy(temp_arr, raw_acc_data[epoch], sizeof(temp_arr));
    struct time_and_value temp_struc;
    int pos_acc_peaks_array = 0;
    for(int i=0;i<sizeof(temp_arr)/sizeof(struct time_and_value);i++){
        if(temp_arr[i].time == 0){
            //printf("end of data reached\n");
            break;
        }
        //first value is bigger than second value, so it is considered a peak
        if (i==0 && temp_arr[i].value > temp_arr[i+1].value){
            //printf("first value is peak\n");
            temp_struc.time = temp_arr[i].time;
            temp_struc.value = temp_arr[i].value;
            acc_peaks[pos_acc_peaks_array] = temp_struc;
            pos_acc_peaks_array++;
        // last value is bigger than second-to-last value, so its considered a peak
        } else if (temp_arr[i+1].time == 0 && temp_arr[i].value > temp_arr[i-1].value){
            //printf("last value is peak\n");
            temp_struc.time = temp_arr[i].time;
            temp_struc.value = temp_arr[i].value;
            acc_peaks[pos_acc_peaks_array] = temp_struc;
            pos_acc_peaks_array++;
        // current value is bigger than next and previous value, so its considered a peak
        } else if (temp_arr[i].value > temp_arr[i-1].value && temp_arr[i].value > temp_arr[i+1].value){
            //printf("pos %d is peak\n",i);
            temp_struc.time = temp_arr[i].time;
            temp_struc.value = temp_arr[i].value;
            acc_peaks[pos_acc_peaks_array] = temp_struc;
            pos_acc_peaks_array++;
        }
        // current value is smaller than next and previous value, so its considered a (downward) peak
        else if (temp_arr[i].value < temp_arr[i-1].value && temp_arr[i].value < temp_arr[i+1].value){
            //printf("pos %d is peak\n",i);
            temp_struc.time = temp_arr[i].time;
            temp_struc.value = temp_arr[i].value;
            acc_peaks[pos_acc_peaks_array] = temp_struc;
            pos_acc_peaks_array++;
        }
    }

    //print values of return array for debug purposes
    // for (int i = 0; i< sizeof(acc_peaks);i++){
    //     if (acc_peaks[i].time == 0){
    //         printf("time is 0 on pos %d\n", i);
    //         i = sizeof(acc_peaks);
    //     } else {
    //         printf("acc_peaks values in pos %d: time: %ld, value: %lf\n", i, acc_peaks[i].time, acc_peaks[i].value);
    //     }
    // }
}

void filter_peaks_on_threshold (int epoch, double mean){
    int pos_filtered_array = 0;
    double upper_bound = mean + ACC_THRESHOLD;
    double lower_bound = mean - ACC_THRESHOLD;
    //printf("upper bound: %lf and lower bound: %lf\n", upper_bound, lower_bound);
    for (int i = 0;i<sizeof(acc_peaks)/sizeof(acc_peaks[0]);i++){
        if (acc_peaks[i].time == 0){
            i = sizeof(acc_peaks);
        } else if (acc_peaks[i].value > upper_bound || acc_peaks[i].value <  lower_bound){
            //printf("peak %d is outside threshold range\n", i);
            filtered_acc_peaks[pos_filtered_array].time = acc_peaks[i].time;
            filtered_acc_peaks[pos_filtered_array].value = acc_peaks[i].value;
            pos_filtered_array++;
        } else {
            //printf("peak %d is inside threshold range\n", i);
        }
    }

    //print for debug purposes
    // for (int i = 0;i<sizeof(filtered_acc_peaks);i++){
    //     if (filtered_acc_peaks[i].time == 0){
    //         i = sizeof(filtered_acc_peaks);
    //     } else {
    //     printf(" in epoch %d, pos %d time is: %ld, value is: %lf\n", epoch, i, filtered_acc_peaks[i].time, filtered_acc_peaks[i].value);
    //     }
    // }
}

void calc_activity_p2p (int epoch){
    // get smallest interval between two successive timestamps
    long int minimal_time_difference = abs(filtered_acc_peaks[0].time);
    if (filtered_acc_peaks[2].time == 0){
        //printf("filtered_acc_peaks does not contain enough peaks\n");
        activity_p2p[epoch] = 0;
    } else {
        for(int i = 0; i<sizeof(filtered_acc_peaks);i++){
            if (filtered_acc_peaks[i+1].time==0){
                i=sizeof(filtered_acc_peaks);
            } else {
                int time_difference = abs(filtered_acc_peaks[i].time - filtered_acc_peaks[i+1].time);
                //printf("time current peak: %ld, time next peak: %ld, difference: %ld\n", filtered_acc_peaks[i].time, filtered_acc_peaks[i+1].time, filtered_acc_peaks[i].time - filtered_acc_peaks[i+1].time);
                //printf("current time difference is: %d\n", time_difference);
                if(minimal_time_difference > time_difference){
                    minimal_time_difference = time_difference;
                    //printf("minimal_time_diff_updated: %ld\n", minimal_time_difference);
                }
            }
        }

        // check if it scores as active
        if (minimal_time_difference<11000){
            activity_p2p[epoch] = 1;
            //printf("Minimal_time_difference: %ld\n", minimal_time_difference);
        } else {
            activity_p2p[epoch] = 0;
        }
    }
    

    // print activity_p2p for debug purposes
    //printf("Activity p2p in epoch %d is: %d\n",epoch, activity_p2p[epoch]);
}

void calculate_MD_p2p (int epoch){
    int total_activity = 0;
    double MD_p2p = 0;
    // the first epoch is epoch 0, not epoch 1 so we use 0 here instead of 1
    if(epoch >= 0 && epoch < WINDOW_SIZE_ACC){
        for (int i = 0; i<=epoch;i++){
            total_activity += activity_p2p[i];
        }
        MD_p2p = (double)total_activity/ (epoch + 1);
        MD_p2p_array[epoch] = MD_p2p;
    } else {
        for( int j = epoch - WINDOW_SIZE_ACC +1 ; j<= epoch; j++){
            total_activity += activity_p2p[j];
            //printf("epoch %d, j: %d, total_activity %d\n",epoch,j, total_activity);
        }
        MD_p2p = (double)total_activity/WINDOW_SIZE_ACC;
        //printf("MD_2p2: %lf\n",MD_p2p);
        MD_p2p_array[epoch] = MD_p2p;

    }
    //printf("MD of p2p in epoch %d is: %lf\n", epoch, MD_p2p_array[epoch]);
}

void calc_activity_max_mag(int epoch, double mean){
    double max_magnitude = 0;
    for(int i=0;i<sizeof(filtered_acc_peaks)/sizeof(struct time_and_value);i++){
        if (filtered_acc_peaks[i].time == 0){
            i = sizeof(filtered_acc_peaks);
        } else {
            double deviation = fabs((double)filtered_acc_peaks[i].value - mean);
            if (deviation > max_magnitude){
                max_magnitude = 1;
                // one value is outside the threshold range, so this epoch is scored as active
                i = sizeof(filtered_acc_peaks);
            }
        }
    }
    activity_max_mag[epoch] = max_magnitude;
}

void calculate_MD_max_mag (int epoch){
    int total_activity = 0;
    double MD_max_mag = 0;
    // the first epoch is epoch 0, not epoch 1 so we use 0 here instead of 1
    if(epoch >= 0 && epoch < WINDOW_SIZE_ACC){
        for (int i = 0; i<=epoch;i++){
            total_activity += activity_max_mag[i];
        }
        MD_max_mag = (double)total_activity/ (epoch + 1);
        MD_max_mag_array[epoch] = MD_max_mag;
    } else {
        for( int i = epoch - WINDOW_SIZE_ACC + 1; i<= epoch; i++){
            total_activity += activity_max_mag[i];
        }
        MD_max_mag = (double)total_activity/WINDOW_SIZE_ACC;
        MD_max_mag_array[epoch] = MD_max_mag;

    }
    //printf("MD of max mag in epoch %d is: %lf\n", epoch, MD_max_mag_array[epoch]);
}

void combine_MDs (int epoch){
    double MDs = (MD_p2p_array[epoch] + MD_max_mag_array[epoch])/2;
    MD_combined[epoch] = MDs;
}


void acc_algorithm(){
    //loop for each epoch:
    for (int epoch = 0; epoch<total_acc_epochs;epoch++){
        if (raw_acc_data[epoch][0].time == 0){
            //printf("time of epoch %d is 0, so end of data reached.\n",epoch);
            break;
        }
        // for (int i = 0; i<1000; i++){
        //     acc_peaks[i].time = 0;
        //     acc_peaks[i].value = 0;
        //     filtered_acc_peaks[i].time = 0;
        //     filtered_acc_peaks[i].value = 0;
        // }
        memset(acc_peaks, 0, sizeof(acc_peaks));
        memset(filtered_acc_peaks, 0, sizeof(filtered_acc_peaks));
        //for (int i = 0; i<sizeof())
        // calculate mean of current epoch
        double mean = calc_mean_acc(epoch);
        //printf("mean of epoch %d is %lf\n",epoch,mean);
        // filter peaks-> put in separate array
        filter_peaks(epoch);
        // filter on threshold
        filter_peaks_on_threshold(epoch, mean);

        //calculate MD of P-to-P
        calc_activity_p2p(epoch);
        calculate_MD_p2p(epoch);

        //calculate MD of Max Mag
        calc_activity_max_mag(epoch, mean);
        calculate_MD_max_mag(epoch);

        //combine both MD,s and store result in the processed data array.
        combine_MDs(epoch);
    }
    // printf("list of moving densities:\np2p:\n");
    // for (int i=0;i<sizeof(MD_p2p_array)/sizeof(MD_p2p_array[0]);i++){
    //     printf("%f, ", MD_p2p_array[i]);
    // }
    // printf("\nmax_mag:\n");
    // for (int i=0;i<sizeof(MD_max_mag_array)/sizeof(MD_max_mag_array[0]);i++){
    //     printf("%f, ", MD_max_mag_array[i]);
    // }
    printf("\ncombined acc: \n");
    for (int i=0;i<total_acc_epochs;i++){
        printf("%f, ", MD_combined[i]);
    }
    printf("\n");
}

double calc_mean_hr(){
    int total = 0;
    double mean = 0;
    int nmb_datapoints = 0;
    for (int epoch = 0; epoch < 4; epoch++){
        for (int i = 0; i<sizeof(raw_hr_data[epoch]);i++){
            if (raw_hr_data[epoch][i].time != 0){
                total += raw_hr_data[epoch][i].value;
                nmb_datapoints++;
            } else {
                i = sizeof(raw_hr_data[epoch]);
            }
        }
        if (epoch == 3){
            mean = (double)total / nmb_datapoints;
            printf("hr total: %d, nmb_datapoints: %d and mean: %f\n", total, nmb_datapoints, mean);
        }
    }
    return mean;
}

double calc_std_dev(double mean){
    double std_dev = 0;
    int nmb_datapoints = 0;
    for (int epoch = 0; epoch < 4; epoch++){
        for (int i = 0; i<sizeof(raw_hr_data[epoch]);i++){
            if (raw_hr_data[epoch][i].time != 0){
                std_dev += pow(raw_hr_data[epoch][i].value - mean, 2);
                nmb_datapoints++;
            } else {
                i = sizeof(raw_hr_data[epoch]);
            }
        }
    }
    return sqrt(std_dev/nmb_datapoints);
}

void heart_rate_algorithm(){
    // calculate threshold
    double mean = calc_mean_hr();
    double std_dev = calc_std_dev(mean);
    printf("std_dev: %f,\n", std_dev);

    double threshold = mean - (5 * std_dev);
    //double threshold = mean - (1.96 * std_dev);
    printf("threshold: %f\n", threshold);

    int sleep_onset_epoch = 0;
    int epoch = 4; // meaning its the fifth epoch
    int consecutive_under_threshold = 0;

    for(epoch;epoch<total_hr_epochs;epoch++){
        int samples_in_current_epoch = 0;
        //calculate samples_in_current_epoch
        for(int i = 0;i<sizeof(raw_hr_data[epoch]);i++){
            if(raw_hr_data[epoch][i].time != 0){
                samples_in_current_epoch++;
            } else {
                i = sizeof(raw_acc_data[epoch]);
            }
        }
        //printf("samples in current epoch: %d\n",samples_in_current_epoch);

        for(int sample=0;sample<samples_in_current_epoch;sample++){
            if (raw_hr_data[epoch][sample].value < threshold){
                consecutive_under_threshold++;

                if (consecutive_under_threshold >= samples_in_current_epoch/2){
                    sleep_onset_epoch = epoch + 1; //off by one correction
                    printf("sleep_onset_epoch: %d\n", sleep_onset_epoch);
                    sample = samples_in_current_epoch;
                    epoch = sizeof(raw_hr_data);
                }
            } else {
                consecutive_under_threshold = 0;
            }
        }     
    }
    if(sleep_onset_epoch>0){
        start_sleep_onset_period = sleep_onset_epoch - 2;
        printf("sleep_onset_period starts at epoch %d\n",start_sleep_onset_period);
    } else{
        printf("The algorithm did not detect a sleep onset epoch\n");
    }
}

int main(int argc, char *argv[]){

    if (argc < 3){
        printf("Missing filename(s)\n");
        return(1);
    }
    else{
        filename_acc = argv[1];
        filename_HR = argv[2];
    }
    //open files
    fp_acc = fopen(filename_acc,"r");
    fp_HR = fopen(filename_HR,"r");
    fp_result = fopen(filename_result,"a+");

    if (fp_acc && fp_HR){
        //retrieve data from file, result: 2 2d arrays with datapoints
        parse_acc_file();
        parse_HR_file();
        
        fclose(fp_acc);
        fclose(fp_HR);

        //execute algorithms on arrays of Acc and HR
        acc_algorithm();
        heart_rate_algorithm();
        //write return of algorithm in a file
        fprintf(fp_result, "acc results:\n");
        for(int i=0;i<total_acc_epochs;i++){
            fprintf(fp_result, "%f, ", MD_combined[i]);
        }
        fprintf(fp_result, "\nHR result: sleep period starts in epoch %d\n\n\n", start_sleep_onset_period);
        //maybe automatically make a visual representation of it.
        fclose(fp_result);


    } else {
        printf("failed to open file\n");
    }

    return 0;
}

